# Possel

## Running
Currently little exists, standard virtualenv stuff applies except we're using python 3 so make your venv with

    virtualenv -p $(which python3) ...

then `pip install -r requirements.txt` in the venv and you can start running it with:

    python -m possel.irc
